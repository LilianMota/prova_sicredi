package appObjects;

import com.aventstack.extentreports.ExtentReports;
import com.sun.org.apache.bcel.internal.generic.Select;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;

public class telaInicialAppObjects {
    private ExtentReports report;
    private telaInicialAppObjects telaInicial;
    private WebDriver driver;

    public telaInicialTask (WebDriver driver) {
        this.driver = driver;
        this.report = report;
        this.telaInicial = new telaInicialAppObjects(driver);
    }

    public telaInicialAppObjects(WebDriver driver) {

    }

    public void trocaVersaoDoBootstrap() {
        Select lista = new Select(telaInicial.getTrocarVersaoSelect());
        lista.selectByValue("bootstrap_theme_v4");
    }

    public void criarNovoCustomer() {
        telaInicial.getAddCustomerButton().click();
    }

    public void pesquisarCustomer(String nomeDoCustomer) {
        telaInicial.getPesquisaNomeTextField().clear();
        telaInicial.getPesquisaNomeTextField().sendKeys(nomeDoCustomer);
        telaInicial.getPesquisaNomeTextField().sendKeys(Keys.ENTER);
    }


    public String verificaNomeDoResultadoDaPesquisa(String nomeDoCustomer) throws InterruptedException {
        int numeroDeResultados = verificaNumeroDeResultadosDaPesquisa(nomeDoCustomer);
        if(numeroDeResultados > 0){
            return telaInicial.getNamePesquisaTextField().getText();
        }
        return "";
    }

    public void deletaCustomer() throws InterruptedException {
        telaInicial.getSelecionarTudoCheckBox().click();
        telaInicial.getDeleteButton().click();
        telaInicial.getDeleteConfirmationButton().click();
    }

    public int verificaNumeroDeResultadosDaPesquisa(String nomeDoCustomer) throws InterruptedException {
        pesquisarCustomer(nomeDoCustomer);
        return Integer.parseInt(telaInicial.getNumeroDeResultadosTextField().getText());
    }
}
