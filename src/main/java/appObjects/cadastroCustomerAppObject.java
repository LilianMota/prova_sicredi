package appObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

public class cadastroCustomerAppObject {
        private WebDriver driver;
        private WebDriverWait wait;
        private Utils utils;

        public cadastroCustomerAppObject(WebDriver driver) {
            this.driver = driver;
            this.utils = new Utils(driver);
            this.wait = new WebDriverWait(driver, 20);
        }



    public void getCadastroForm() throws InterruptedException {
            wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("#crudForm")));
            utils.esperaJSCarregar(By.cssSelector("#crudForm"));
            Thread.sleep(3000);
        }

        public WebElement getNomeTextField() {
            return utils.esperaJSCarregar(By.id("field-customerName"));
        }

        public WebElement getLastNameTextField() {
            return utils.esperaJSCarregar(By.id("field-contactLastName"));
        }

        public WebElement getContactFirstName() {
            return utils.esperaJSCarregar(By.id("field-contactFirstName"));
        }

        public WebElement getPhoneTextField() {
            return driver.findElement(By.id("field-phone"));
        }

        public WebElement getAddressLine1TextField() {
            return driver.findElement(By.id("field-addressLine1"));
        }

        public WebElement getAddressLine2TextField() {
            return driver.findElement(By.id("field-addressLine2"));
        }

        public WebElement getCityTextField() {
            return driver.findElement(By.id("field-city"));
        }

        public WebElement getStateTextField() {
            return driver.findElement(By.id("field-state"));
        }

        public WebElement getPostalCodeTextField() {
            return driver.findElement(By.id("field-postalCode"));
        }

        public WebElement getCountryTextField() {
            return driver.findElement(By.id("field-country"));
        }


        public WebElement getEmployerSelect() {
            return driver.findElement(By.id("field_salesRepEmployeeNumber_chosen"));
        }


        public List<WebElement> getListaDeEmployees() {
            return driver.findElements(By.className("active-result"));
        }


        public WebElement getCreditLimitTextField() {
            return driver.findElement(By.id("field-creditLimit"));
        }

        public WebElement getSaveButton() {
            return driver.findElement(By.id("form-button-save"));
        }

        //Your data has been successfully stored into the database.
        public WebElement getSuccessMessageTextField() {
            return utils.esperaJSCarregar(By.id("report-success"));
        }

        public WebElement getGoBackToListButton() {
            return utils.esperaJSCarregar(By.cssSelector("#crudForm > div > #report-success > p > a:nth-child(2)"));
        }

    }

