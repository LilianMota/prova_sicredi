package Tasks;

import appObjects.cadastroCustomerAppObject;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.io.IOException;
import java.util.List;
import java.util.Properties;

public class cadastroCustomerTask {
    private cadastroCustomerAppObject cadastro;
    private WebDriver driver;

    public cadastroCustomerTask(WebDriver driver) {
        this.driver = driver;
        this.cadastro = new cadastroCustomerAppObject(driver);
    }

    public void cadastroDeCustomer(Properties propriedades) throws InterruptedException {

        cadastro.getCadastroForm();

        System.out.println(propriedades.getProperty("name"));
        cadastro.getNomeTextField().sendKeys(propriedades.getProperty("name"));

        System.out.println(propriedades.getProperty("lastName"));
        cadastro.getLastNameTextField().sendKeys(propriedades.getProperty("lastName"));

        cadastro.getContactFirstName().sendKeys(propriedades.getProperty("contactFirstName"));

        cadastro.getPhoneTextField().sendKeys(propriedades.getProperty("phone"));

        cadastro.getAddressLine1TextField().sendKeys(propriedades.getProperty("addressLine1"));

        cadastro.getAddressLine2TextField().sendKeys(propriedades.getProperty("addressLine2"));

        cadastro.getCityTextField().sendKeys(propriedades.getProperty("city"));

        cadastro.getStateTextField().sendKeys(propriedades.getProperty("state"));

        cadastro.getPostalCodeTextField().sendKeys(propriedades.getProperty("postalCode"));

        cadastro.getCountryTextField().sendKeys(propriedades.getProperty("country"));

        // clica pra lista de employeer aparecer
        cadastro.getEmployerSelect().click();
        //seleciona o employeer que tu quer
        selecionaItemDaListaDeEmployees(propriedades.getProperty("employeer"));

        cadastro.getCreditLimitTextField().sendKeys(propriedades.getProperty("creditLimit"));

        cadastro.getSaveButton().click();
    }

    private void selecionaItemDaListaDeEmployees(String item) {
        List<WebElement> elementos = cadastro.getListaDeEmployees();
        for(WebElement elemento : elementos) {
            if(elemento.getText().contentEquals(item)) {
                elemento.click();
            }
        }
    }


    public void selecionaVoltarParaLista() {
        cadastro.getGoBackToListButton().click();
    }

    public String verificaUsuarioCadastrado() throws IOException {
        return cadastro.getSuccessMessageTextField().getText();
    }

}
