package Utils;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.FileInputStream;
import java.io.IOException;

public class Utils {
    private WebDriver driver;

    public Utils(WebDriver driver){
        this.driver = driver;
    }

    public Properties getProperties(String nomeDoArquivo) throws IOException {
        InputStream inputStream = null;
        Properties prop = new Properties();
        try {

            prop = new Properties();

            File file = new File(System.getProperty("user.dir") + "/src/resources/" + nomeDoArquivo);
            inputStream = new FileInputStream(file);
            prop.load(inputStream);
            return prop;

        } catch (Exception e) {
            System.out.println("Exception: " + e);
        } finally {
            inputStream.close();
        }
        return prop;
    }

    public WebElement elementoExiste(By tipoElemento){
        try{
            return esperaJSCarregar(tipoElemento);
        }catch(NoSuchElementException notFound){
            System.out.println(notFound.getMessage());
        }catch(Exception e){
            System.out.println(e.getMessage());
        }
        return null;
    }

    public WebElement esperaJSCarregar(By identificadorDoElemento){
        boolean carregou = false;
        WebDriverWait wait = new WebDriverWait(driver, 1000);
        AditionalConditions addConditions = new AditionalConditions(driver);
        while(!carregou){
            carregou = addConditions.esperaAjaxEJqueryCarregar();
        }
        return wait.until(ExpectedConditions.visibilityOf(driver.findElement(identificadorDoElemento)));
    }

}
