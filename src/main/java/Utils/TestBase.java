package Utils;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.reporter.ExtentSparkReporter;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;

public class TestBase {
    protected Utils utils;
    protected Properties informacoes;
    protected ExtentReports report = new ExtentReports();
    protected ExtentSparkReporter spark = new ExtentSparkReporter(System.getProperty("user.dir") + "/report/");

    public WebDriver getDriver(){
        return DriverManagerFactory.setDriver(DriverType.CHROME);
    }

    public void getReport() {
        report.attachReporter(spark);
    }

    @BeforeSuite
    public void configuraReport(){
        report.attachReporter(spark);
    }

    @AfterSuite
    public void tearDown() {
        report.flush();
        DriverManagerFactory.quitDriver();
    }

}
