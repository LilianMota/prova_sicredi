package TesteCase;

import Tasks.cadastroCustomerTask;
import Tasks.telaInicialTask;
import Utils.TestBase;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.io.IOException;

public class TestCase extends TestBase {
    private WebDriver driver = getDriver();
    private telaInicialTask telaInicial = new telaInicialTask(driver);
    private cadastroCustomerTask cadastroCustomer = new cadastroCustomerTask(driver);

    @BeforeMethod
    public void setUp() throws IOException {
        utils = new Utils(driver);
        informacoes = utils.getProperties("infoCadastro.properties");
        driver.get(informacoes.getProperty("linkAplicacao"));
    }

    @Test
    public void CriarNovoCustomer() throws IOException, InterruptedException {

        ExtentTest criacaoDeCustomer = report.createTest("Criação de customer");

        try{
            telaInicial.trocaVersaoDoBootstrap();
            telaInicial.criarNovoCustomer();

            cadastroCustomer.cadastroDeCustomer(informacoes);

            String mensagem = cadastroCustomer.verificaUsuarioCadastrado();

            Assert.assertEquals(mensagem,"Your data has been successfully stored into the database. Edit Customer or Go back to list");
            criacaoDeCustomer.log(Status.PASS, "Customer cadastrado com sucesso!");
        }catch(AssertionError ae){
            criacaoDeCustomer.log(Status.FAIL, "Ocorreu um erro ao cadastrar o customer: " + ae.getMessage());
        }catch(Exception e){
            criacaoDeCustomer.log(Status.FAIL, "Ocorreu um erro durante a execução! " + e.getMessage());
        }
    }

    @Test
    public void CriarEDeletarCustomer() throws IOException, InterruptedException {

        ExtentTest deletarNovoCustomer = report.createTest("Deletar novo customer");

        try{
            telaInicial.trocaVersaoDoBootstrap();
            telaInicial.criarNovoCustomer();

            cadastroCustomer.cadastroDeCustomer(informacoes);

            String mensagemCadastro = cadastroCustomer.verificaUsuarioCadastrado();
            Assert.assertEquals(mensagemCadastro, "Your data has been successfully stored into the database. Edit Customer or Go back to list");

            cadastroCustomer.selecionaVoltarParaLista();

            String nomeDoCustomer = informacoes.getProperty("name");

            String nomeDoResultado = telaInicial.verificaNomeDoResultadoDaPesquisa(nomeDoCustomer);
            Assert.assertEquals(nomeDoResultado, nomeDoCustomer);

            telaInicial.deletaCustomer();

            int numeroDeResultados = telaInicial.verificaNumeroDeResultadosDaPesquisa(nomeDoCustomer);
            Assert.assertEquals(numeroDeResultados, 0);
            deletarNovoCustomer.log(Status.PASS, "Customer deletado com sucesso!");

        }catch(AssertionError ae){
            deletarNovoCustomer.log(Status.FAIL, "Ocorreu um erro ao deletar o customer: " + ae.getMessage());
        }catch(Exception e){
            deletarNovoCustomer.log(Status.FAIL, "Ocorreu um erro durante a execução! " + e.getMessage());
        }
    }


}
